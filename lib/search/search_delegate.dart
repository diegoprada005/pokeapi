//import 'package:flutter/material.dart';

//class MovieSearchDelegate extends SearchDelegate {
//  @override
//  String get searchFieldLabel => 'Buscar Pokemon';
//  //funcion para colocar un mensaje de fondo en el nput

//  @override
//  List<Widget> buildActions(BuildContext context) {
//    return [
//      IconButton(
//        icon: const Icon(Icons.clear),
//        onPressed: () => query = '', // vaciar lo que haya en el input
//      )
//    ];
//  }

//  @override
//  Widget buildLeading(BuildContext context) {
//    return IconButton(
//      icon: const Icon(Icons.arrow_back),
//      onPressed: () {
//        close(context, null);
//        // devolveme una ventana
//      },
//    );
//  }

//  @override
//  Widget buildResults(BuildContext context) {
//    return const Text('buildResults');
//  }

//  Widget _emptyContainer() {
//    return const Center(
//      child: Icon(
//        Icons.movie_creation_outlined,
//        // para colocar el icono de fondo
//        color: Colors.black38,
//        size: 130,
//      ),
//    );
//  }

//  @override
//  Widget buildSuggestions(BuildContext context) {
//    //$query viene de SearchDelegate y es para capturar lo que se esta escribiendo el input y reflejarlo en la busqueda de resultados
//    if (query.isEmpty) {
//      return _emptyContainer();
//    }
//    print('http request');

//   final moviesProvider = Provider.of<MoviesProvider>(context, listen: true);
//    // evistar que se este redibujando
//    moviesProvider.getSuggestionsByQuery(query);

//    return StreamBuilder(
//      //StreamBuilder puedo controlar el stream y el flujo
//      stream: moviesProvider.suggestionStream,
//      builder: (_, AsyncSnapshot<List<Movie>> snapshot) {
//        if (!snapshot.hasData) return _emptyContainer();
//        // pregunta si hay data

//        final movies = snapshot.data!;

//        return ListView.builder(
//            itemCount: movies.length,
//            itemBuilder: (_, int index) => _MovieItem(movies[index]));
//      },
//    );
//  }
//}

//class _MovieItem extends StatelessWidget {
//  final Movie movie;

//  const _MovieItem(this.movie);

//  @override
//  Widget build(BuildContext context) {
//    movie.heroId = 'search-${movie.id}';

//    return ListTile(
//      leading: Hero(
//        tag: movie.heroId!,
//        child: FadeInImage(
//          placeholder: const AssetImage('assets/no-image.jpg'),
//          image: NetworkImage(movie.fullPosterImg),
//          width: 50,
//          fit: BoxFit.contain,
//        ),
//      ),
//      title: Text(movie.title),
//      subtitle: Text(movie.originalTitle),
//      onTap: () {
//        Navigator.pushNamed(context, 'details', arguments: movie);
//      },
//    );
//  }
//}
