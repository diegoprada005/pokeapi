import 'package:flutter/material.dart';
import 'package:pokemons/widgets/options_button.dart';
import 'package:provider/provider.dart';

import 'package:pokemons/provider/provider.dart';

class DescriptionScreen extends StatelessWidget {
  const DescriptionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final button = Provider.of<ThemeProvider>(context);

    return Scaffold(
        body: Column(
      children: [
        const _Header(),
        const _Body(),
        if (button.buttonNumber == 0) const About(),
        if (button.buttonNumber == 1) const Stats(),
        if (button.buttonNumber == 2) const Evolutions(),
      ],
    ));
  }
}

class _Header extends StatelessWidget {
  const _Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colors.black),
            color: Colors.red,
            borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(120),
                bottomRight: Radius.circular(120))),
        height: MediaQuery.of(context).size.height * 0.4,
        width: double.infinity,
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(18),
              margin: EdgeInsets.only(
                  right: MediaQuery.of(context).size.width * 0.9),
              child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(Icons.arrow_back)),
            )
          ],
        ));
  }
}

class _Body extends StatelessWidget {
  const _Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 80,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          _ElevatedButton(title: 'About', index: 0),
          _ElevatedButton(title: 'Status', index: 1),
          _ElevatedButton(title: 'Evolutions', index: 2)
        ],
      ),
    );
  }
}

class _ElevatedButton extends StatelessWidget {
  final int index;
  final String title;
  const _ElevatedButton({
    Key? key,
    required this.title,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final button = Provider.of<ThemeProvider>(context);
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(primary: Colors.white),
        onPressed: () {
          button.buttonNumber = index;
        },
        child: Text(title,
            style: TextStyle(
                color: button.buttonNumber == index
                    ? Colors.deepOrange
                    : Colors.grey[400])),
      ),
    );
  }
}
