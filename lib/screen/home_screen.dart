import 'package:flutter/material.dart';
import 'package:pokemons/models/pokemonInfo.dart';
import 'package:pokemons/screen/loading_screen.dart';

import 'package:provider/provider.dart';
import 'package:pokemons/provider/provider.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ApiProvider>(context);
    if (provider.isLoading) return const LoadingScreen();

    return Scaffold(
      body: Column(
        children: [
          Stack(
            children: const [
              _Background(),
              _Header(),
            ],
          ),
          const _Body(),
        ],
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        _Title(),
        _Search(),
      ],
    );
  }
}

class _Background extends StatelessWidget {
  const _Background({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 200),
      height: 200,
      width: 350,
      child: const Image(image: AssetImage('assets/pokebola.png')),
    );
  }
}

class _Title extends StatelessWidget {
  const _Title({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeProvider>(context);

    return Container(
        margin: const EdgeInsets.symmetric(vertical: 80),
        padding: const EdgeInsets.only(right: 190),
        child: Text('Pokédex',
            style: TextStyle(
                color: theme.isActive ? Colors.red : Colors.white,
                fontSize: 40,
                fontWeight: FontWeight.bold)));
  }
}

class _Search extends StatelessWidget {
  const _Search({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mode = Provider.of<ThemeProvider>(context);
    return Column(
      children: [
        Row(
          children: [
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 30),
              padding: const EdgeInsets.only(right: 90),
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(10)),
              height: 30,
              child: Row(
                children: const [
                  SizedBox(width: 10),
                  Icon(Icons.search),
                  Text('Buscar pokemon'),
                ],
              ),
            ),
            Expanded(
              child: SwitchListTile.adaptive(
                  value: mode.isActive,
                  onChanged: (value) {
                    mode.isActive = value;
                  }),
            )
          ],
        ),
      ],
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pokemonProvier = Provider.of<ApiProvider>(context);

    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.2,
      width: double.infinity,
      child: GestureDetector(
          onTap: () =>
              Navigator.popAndPushNamed(context, 'description', arguments: ''),
          child: ListView.builder(
              itemCount: pokemonProvier.pokemondetails.length,
              itemBuilder: (_, index) => Container(
                  margin: const EdgeInsets.only(top: 10),
                  child: _TargetPokemon(
                      pokemonProvier: pokemonProvier, index: index)))),
    );
  }
}

class _TargetPokemon extends StatelessWidget {
  final int index;
  final ApiProvider pokemonProvier;

  const _TargetPokemon({
    Key? key,
    required this.pokemonProvier,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PokemonsDetails? pokemonsDetails;

    return Card(
        color: Colors.blue,
        margin: const EdgeInsets.symmetric(horizontal: 20),
        clipBehavior: Clip.antiAlias,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15),
                topRight: Radius.circular(15),
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
        child: Column(
          children: [
            Row(
              children: [
                const SizedBox(width: 20),
                Container(
                  margin: const EdgeInsets.only(top: 40),
                ),

                Expanded(
                  child: SizedBox(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height * 0.1,
                    child: Image.network(pokemonsDetails!
                        .sprites!.other!.officialArtwork!.frontDefault!),
                    //FadeInImage(
                    //  placeholder: const AssetImage('assets/no-image.jpg'),
                    //  image: NetworkImage(pokemonsDetails!
                    //          .sprites!.other!.officialArtwork!.frontDefault!
                    //      //pokemonsDetails?.nullImage == null
                    //      //      ? 'https://i.stack.imgur.com/GNhxO.png'
                    //      //      : pokemonsDetails?.nullImage
                    //      //fit: BoxFit.cover,
                    //      ),
                    //),
                  ),
                ),
                //const Image(
                //    width: 90,
                //    height: 40,
                //    fit: BoxFit.cover, //TODO: IMAGEN POKEMON
                //    image: AssetImage('assets/pokemon.jpg')),
                const SizedBox(width: 30),
                Text(pokemonProvier.pokemondetails[index].ability.name,
                    // pokemonProvier.pokemondetails[index].ability.name,
                    style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white)),
              ],
            ),
            Container(
              margin: const EdgeInsets.only(left: 270),
              child: Opacity(
                opacity: 0.6,
                child: Text(
                    index < 10 ? '00$index' : '$index', //dataPokemon.num
                    style: const TextStyle(
                        fontSize: 50,
                        fontWeight: FontWeight.bold,
                        color: Colors.white)),
              ),
            )
          ],
        ));
  }
}
