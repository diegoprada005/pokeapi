// ignore_for_file: file_names

import 'dart:convert';

class PokemonsDetails {
  PokemonsDetails({
    required this.abilities,
    required this.height,
    required this.id,
    required this.name,
    required this.species,
    this.sprites,
    required this.stats,
    required this.types,
    required this.weight,
  });

  List<Ability> abilities;
  int height;
  int id;
  String name;
  Species species;
  Sprites? sprites;
  List<Stat> stats;
  List<Type> types;
  int weight;

  get nullImage {
    if (sprites != null) {
      return sprites;
    } else {
      return 'https://i.stack.imgur.com/GNhxO.png';
    }
  }

  factory PokemonsDetails.fromJson(String str) =>
      PokemonsDetails.fromMap(json.decode(str));

  factory PokemonsDetails.fromMap(Map<String, dynamic> json) => PokemonsDetails(
        abilities: List<Ability>.from(
            json["abilities"].map((x) => Ability.fromMap(x))),
        height: json["height"],
        id: json["id"],
        name: json["name"],
        species: Species.fromMap(json["species"]),
        sprites: Sprites.fromJson(json["sprites"]),
        stats: List<Stat>.from(json["stats"].map((x) => Stat.fromMap(x))),
        types: List<Type>.from(json["types"].map((x) => Type.fromMap(x))),
        weight: json["weight"],
      );
}

class Ability {
  Ability({
    required this.ability,
    required this.isHidden,
    required this.slot,
  });

  Species ability;
  bool isHidden;
  int slot;

  factory Ability.fromJson(String str) => Ability.fromMap(json.decode(str));

  factory Ability.fromMap(Map<String, dynamic> json) => Ability(
        ability: Species.fromMap(json["ability"]),
        isHidden: json["is_hidden"],
        slot: json["slot"],
      );
}

class Species {
  Species({
    required this.name,
    required this.url,
  });

  String name;
  String url;

  factory Species.fromJson(String str) => Species.fromMap(json.decode(str));

  factory Species.fromMap(Map<String, dynamic> json) => Species(
        name: json["name"],
        url: json["url"],
      );
}

class Sprites {
  Other? other;

  Sprites({this.other});

  factory Sprites.fromJson(Map<String, dynamic> json) => Sprites(
        other: json["other"] == null ? null : Other.fromJson(json["other"]),
      );

  Map<String, dynamic> toJson() => {
        "other": other == null ? null : other!.toJson(),
      };
}

class OfficialArtwork {
  String? frontDefault;

  OfficialArtwork({this.frontDefault});

  factory OfficialArtwork.fromJson(Map<String, dynamic> json) =>
      OfficialArtwork(
        frontDefault: json["front_default"],
      );

  Map<String, dynamic> toJson() => {
        "front_default": frontDefault,
      };
}

class Other {
  OfficialArtwork? officialArtwork;

  Other({this.officialArtwork});

  factory Other.fromJson(Map<String, dynamic> json) => Other(
        officialArtwork: OfficialArtwork.fromJson(json["official-artwork"]),
      );

  Map<String, dynamic> toJson() => {
        "official-artwork": officialArtwork!.toJson(),
      };
}

class Stat {
  Stat({
    required this.baseStat,
    required this.effort,
    required this.stat,
  });

  int baseStat;
  int effort;
  Species stat;

  factory Stat.fromJson(String str) => Stat.fromMap(json.decode(str));

  factory Stat.fromMap(Map<String, dynamic> json) => Stat(
        baseStat: json["base_stat"],
        effort: json["effort"],
        stat: Species.fromMap(json["stat"]),
      );
}

class Type {
  Type({
    required this.slot,
    required this.type,
  });

  int slot;
  Species type;

  factory Type.fromJson(String str) => Type.fromMap(json.decode(str));

  factory Type.fromMap(Map<String, dynamic> json) => Type(
        slot: json["slot"],
        type: Species.fromMap(json["type"]),
      );
}
