// To parse this JSON data, do
//
//     final pokemons = pokemonsFromMap(jsonString);

import 'dart:convert';

class Pokemons {
  Pokemons({
    required this.results,
  });

  List<Result> results;

  factory Pokemons.fromJson(String str) => Pokemons.fromMap(json.decode(str));

  factory Pokemons.fromMap(Map<String, dynamic> json) => Pokemons(
        results:
            List<Result>.from(json["results"].map((x) => Result.fromMap(x))),
      );
}

class Result {
  Result({
    required this.name,
    required this.url,
  });

  String name;
  String url;

  factory Result.fromJson(String str) => Result.fromMap(json.decode(str));

  factory Result.fromMap(Map<String, dynamic> json) => Result(
        name: json["name"],
        url: json["url"],
      );
}
