import 'package:flutter/material.dart';

import 'package:pokemons/provider/provider.dart';
import 'package:pokemons/screen/screen.dart';
import 'package:provider/provider.dart';

void main() async {
  runApp(const AppState());
}

class AppState extends StatelessWidget {
  const AppState({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider(create: (_) => ThemeProvider(), lazy: false),
      ChangeNotifierProvider(create: (_) => ApiProvider(), lazy: false),
    ], child: const MyApp());
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeProvider>(context);
    //SystemChrome.setSystemUIOverlayStyle(
    //  systemba
    //);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const Scaffold(
        body: Center(
          child: Text('Hello World'),
        ),
      ),
      initialRoute: 'start',
      routes: {
        'start': (_) => const HomeScreen(),
        'description': (_) => const DescriptionScreen()
      },
      theme: theme.isActive ? ThemeData.light() : ThemeData.dark(),
    );
  }
}
