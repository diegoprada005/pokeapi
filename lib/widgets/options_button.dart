import 'package:flutter/material.dart';

class About extends StatelessWidget {
  const About({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: const [
          SizedBox(height: 20),
          _Text(
              text:
                  'loremId veniam exercitation adipisicing mollit excepteur nostrud laborum velit duis esse minim veniam ullamco consectetur.',
              color: Colors.black),
          SizedBox(height: 30),
          _Text(text: 'Pokedex Data.', color: Colors.deepOrange),
          SizedBox(height: 30),
          _Text(
              text:
                  'loremId veniam exercitation adipisicing mollit excepteur nostrud laborum velit duis esse minim veniam ullamco consectetur.',
              color: Colors.black)
        ],
      ),
    );
  }
}

class Stats extends StatelessWidget {
  const Stats({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const _Text(text: 'Base Status', color: Colors.deepOrange),
          const _Statistics(statistics: 'HP:        ', power: 45 / 100),
          const _Statistics(statistics: 'Attack:  ', power: 75 / 100),
          const _Statistics(statistics: 'Defense:', power: 35 / 100),
          const _Statistics(statistics: 'Sp. Atk: ', power: 63 / 100),
          const _Statistics(statistics: 'Sp Def:  ', power: 45 / 100),
          const _Statistics(statistics: 'Speed:   ', power: 20 / 100),
          Column(
            children: [
              Stack(
                children: const [
                  _Background(top: 10, left: 0, width: 80),
                  _Background(top: 90, left: 125, width: 100),
                  _Background(top: 190, left: 305, width: 110)
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}

class _Text extends StatelessWidget {
  final String text;
  final Color color;
  const _Text({
    Key? key,
    required this.text,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(fontSize: 20, color: color),
    );
  }
}

class _Statistics extends StatelessWidget {
  final double power;
  final String statistics;
  const _Statistics({Key? key, required this.statistics, required this.power})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _Text(text: statistics, color: Colors.black),
        const _Text(text: '', color: Colors.black),
        const SizedBox(width: 10),
        Container(
          decoration: BoxDecoration(
              border: Border.all(width: 1, color: Colors.grey[400]!),
              borderRadius: BorderRadius.circular(20)),
          height: 9,
          width: MediaQuery.of(context).size.width * 0.4,
          child: LinearProgressIndicator(
            color: power < 0.5 ? Colors.white : Colors.deepOrange[300],
            value: power,
            backgroundColor: Colors.grey[400],
          ),
        )

        //
      ],
    );
  }
}

class _Background extends StatelessWidget {
  final double top;
  final double left;
  final double width;
  const _Background({
    Key? key,
    required this.top,
    required this.left,
    required this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: top, left: left),
      child: RotationTransition(
        turns: const AlwaysStoppedAnimation(70 / 360),
        child: SizedBox(
            width: width,
            child: const Image(image: AssetImage('assets/pokebola.png'))),
      ),
    );
  }
}

class Evolutions extends StatelessWidget {
  const Evolutions({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              margin: const EdgeInsets.only(top: 20, left: 30),
            ),
            const PokemonsEvolutions(),
            Container(
                decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Colors.grey),
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(20)),
                height: 5,
                width: 80),
            const PokemonsEvolutions(),
          ],
        ),
        const SizedBox(height: 30),
        Row(
          children: [
            Container(margin: const EdgeInsets.only(top: 20, left: 30)),
            const PokemonsEvolutions(),
            Container(
                decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Colors.grey),
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(20)),
                height: 5,
                width: 80),
            const PokemonsEvolutions(),
          ],
        ),
      ],
    );
  }
}

class PokemonsEvolutions extends StatelessWidget {
  const PokemonsEvolutions({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 150,
        color: Colors.red,
        child: Image.asset('assets/Pikachu2.jpg'));
  }
}
