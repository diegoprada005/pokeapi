import 'package:flutter/material.dart';

class ThemeProvider with ChangeNotifier {
  bool _isActive = true;

  bool get isActive => _isActive;

  set isActive(bool theme) {
    _isActive = theme;

    notifyListeners();
  }

//---------------------Boton Menu----------------
  int _buttonNumber = 2;

  int get buttonNumber => _buttonNumber;
  set buttonNumber(int i) {
    _buttonNumber = i;
    notifyListeners();
  }
}
