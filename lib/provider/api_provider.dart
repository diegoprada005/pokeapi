import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:pokemons/models/pokemon.dart';
import 'package:pokemons/models/pokemonInfo.dart';

class ApiProvider with ChangeNotifier {
  final String _baseURL = 'https://pokeapi.co/api/v2/pokemon?limit=20&offset=0';
  List<Result> pokemonsList = [];

  List<Ability> pokemondetails = [];
  bool isLoading = true;

  ApiProvider() {
    getPokemons();
  }

  Future getPokemons() async {
    //Future getpokemonsid(i) async {

    final resp = await http.get(Uri.parse(_baseURL));

    final pokemon = Pokemons.fromJson(resp.body);

    print(pokemon.results[0].name);
    pokemonsList = pokemon.results;

    for (var i in pokemon.results) {
      await getPokemonsId(url: i.url);
    }

    notifyListeners();
  }

  Future<dynamic> getPokemonsId({required String url}) async {
    final resp = await http.get(Uri.parse(url));

    final pokemon = PokemonsDetails.fromJson(resp.body);
    print(pokemon.sprites!.other!.officialArtwork!.frontDefault);
    pokemondetails = pokemon.abilities;

    print(pokemon.name);
    isLoading = false;
    notifyListeners();
    //print(pokemondetails);
  }
}
