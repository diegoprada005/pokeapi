import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:pokemons/models/pokemon.dart';

class ApiProvider with ChangeNotifier {
  List<Result> pokemonsList = [];
  bool isLoading = true;

  ApiProvider() {
    getPokemons();
  }

  Future getPokemons() async {
    //const url = 'https://pokeapi.co/api/v2/pokemon/';
    //// 'https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json';

    //final resp = await http.get(Uri.parse(url));

    //final pokemon = Result.fromJson(resp.body);

    Future getpokemonsid(i) async {
      const url = 'https://pokeapi.co/api/v2/pokemon/';

      final resp = await http.get(Uri.parse(url));

      final pokemon = Pokemons.fromJson(resp.body);
      pokemonsList = pokemon.results;
      //print(pokemon.results[0].name);
    }

    for (int i = 1; i <= 20; i++) {
      final data = await getpokemonsid(i);
      // print(data);
      pokemonsList = data;
    }
    //print('Pokemon: $pokemonsList');

    notifyListeners();
  }
}
